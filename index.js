// at first run eth testnet server: `npm run eth-rpc-server`
/**
 * Testnet account
 *
 * testnet network
 * {
 *      address: '0xaE3235078E4d5c113D354DB9a7A42E8Dd89957c4',
 *      privateKey: '0x566ea10fcf83a10b25a00801ee63f9289e5ff2d412d863d3f380791fea9c55d0',
 * }
 *
 * rinkeby network (tato ma nejake ethers)
 * {
 *      address: '0x4a7099C0FccF6356Cf1F2839f40FA682b5C061d8',
 *      privateKey: '0x6a4b4842e4bd04b0b41b51cfb815e701da75caf4b4b75d901ee303500eafeb22',
 * }
 *
 * rinkeby network 2
 * {
 *     address: '0x1949acB769Be99b261a5ddb7F0a672f4A7F0369A',
 *     privateKey: '0x7d12805cd7ba1a588af53c5242358d3605f10009309de09975b847a6a815537d',
 * }
 *
 */
const Web3 = require('web3');
const web3 = new Web3();
web3.setProvider(new web3.providers.HttpProvider('http://127.0.0.1:8545'));
const DEFAULT_ADDRESS = '0x4a7099C0FccF6356Cf1F2839f40FA682b5C061d8' // rinkeby network

// createAccount()
// setDefaultAddress();
getAccount();
getBalance();
// getLatestGasLimit();
// sendTransaction('0x4a7099C0FccF6356Cf1F2839f40FA682b5C061d8', '0x1949acB769Be99b261a5ddb7F0a672f4A7F0369A', '0.01')


function getBalance(address = DEFAULT_ADDRESS) {
    return web3.eth.getBalance(address).then((balance) => {
        console.log('getBalance', balance);
        // console.log('balance1', web3.toBigNumber(balance));
        // console.log('balance2', web3.toBigNumber(balance).plus(21).toString(10));
        return balance;
    });
}

function setDefaultAddress(address = DEFAULT_ADDRESS) {
    // if (!web3.eth.defaulAccount) {
        web3.eth.defaultAccount = web3.eth.coinbase = address;
    // }
}

function getBlock(blockNumber) {
    return web3.eth.getBlock(blockNumber).then((blockInfo) => {
        console.log('blockInfo', blockInfo);
        return blockInfo;
    });
}

function checkAllBalances() {
    var totalBal = 0;
    console.log('web3.eth.defaultAccount', web3.eth.defaultAccount);
        console.log('web3.eth.coinbase ', web3.eth.coinbase);
    for (var acctNum in web3.eth.accounts) {
        var acct = web3.eth.accounts[acctNum];
        var acctBal = web3.fromWei(web3.eth.getBalance(acct), "ether");
        totalBal += parseFloat(acctBal);
        console.log("  web3.eth.accounts[" + acctNum + "]: \t" + acct + " \tbalance: " + acctBal + " ether");
    }
    console.log("  Total balance: " + totalBal + " ether");
};

function createAccount() {
    const entropy = web3.utils.randomHex(48);
    const account = web3.eth.accounts.create(entropy);
    console.log('account', account);
    return account;
}

function getAccount(privateKey = '0x566ea10fcf83a10b25a00801ee63f9289e5ff2d412d863d3f380791fea9c55d0') {
    const account = web3.eth.accounts.privateKeyToAccount(privateKey);
    console.log('account', account);
    return account;
}

function getGasPrice() {
    return web3.eth.getGasPrice()
        .then((gas) => {
            console.log('gas price', result)
            return gas;
        });
}

function checkNetworkConnection() {
    return web3.eth.net.isListening()
        .then(() => console.log('is connected'))
        .catch(e => console.log('Wow. Something went wrong'));
}

function getNetworkId() {
    return web3.eth.net.getId()
        .then((id) => {
            console.log('net id', id);
            return id;
        });
}

// TODO: dorobit
function sendTransaction(from, to, amountInEther) {
    if (!amountInEther || !from || !to) throw new Error('sendTransaction args missing');

    return web3.eth.sendTransaction({
            from,
            to,
            value: web3.utils.toWei(amountInEther, 'ether')
        })
        .then((result) => {
            console.log('result', result);
            // getTransaction(tx)
            return result;
        })
        .catch((err) => {
          console.log(err);
        })
}

function getTransaction(tx) {
    return web3.eth.getTransaction(tx)
        .then((tx) => {
            console.log('tx', tx);
            return tx;
        })
}

function getLatestGasLimit() {
    return web3.eth.getBlock('latest').then((latestBlock) => {
        console.log(latestBlock);
        // console.log('gas', gas);
        return latestBlock.gasLimit;
    });
}